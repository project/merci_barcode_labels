<?php

/**
 * Implementation of hook_views_plugins
 */
function merci_barcode_labels_views_plugins() {
  $views_path = drupal_get_path('module', 'views');
  return array(
    'display' => array(
      // Parents are not really displays, just needed so the files can
      // be included.
      'parent' => array(
        'no ui' => TRUE,
        'handler' => 'views_plugin_display',
        'path' => "$views_path/plugins",
        'parent' => '',
      ),
     'attachment' => array(
       'no ui' => TRUE,
       'handler' => 'views_plugin_display_attachment',
       'path' => "$views_path/plugins",
       'parent' => 'parent',
     ),
     'mailing' => array(
       'title' => t('Barcode Labels'),
       'help' => t('Add barcode image support to mailing labels.'),
       'handler' => 'merci_barcode_labels_plugin_display_attachment',
       'parent' => 'attachment',
       'theme' => 'views_view',
       'admin' => t('Barcode Labels'),
       'help topic' => 'barcode-labels',
     ), 
        
  ), 
  );
}

function merci_barcode_labels_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'merci_barcode_labels'),
     ),
    'handlers' => array(
      'merci_barcode_labels_handler_field' => array(
        'parent' => 'content_handler_field_multiple',
      ),
    ),
  );
}
 
