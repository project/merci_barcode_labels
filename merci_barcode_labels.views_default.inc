<?php

/**
 * @file
 * merci barcode labels view 
 */


/**
 * Implementation of hook_views_default_views().
 */
function merci_barcode_labels_views_default_views() {

  $view = new view;
  $view->name = 'merci_barcode_labels_print';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
	'title' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'exclude' => 0,
	  'id' => 'title',
	  'table' => 'node',
	  'field' => 'title',
	  'relationship' => 'none',
	),
	'field_merci_barcode_barcode' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'label_type' => 'none',
	  'format' => 'plain',
	  'multiple' => array(
		'group' => TRUE,
		'multiple_number' => '',
		'multiple_from' => '',
		'multiple_reversed' => FALSE,
	  ),
	  'exclude' => 0,
	  'id' => 'field_merci_barcode_barcode',
	  'table' => 'node_data_field_merci_barcode',
	  'field' => 'field_merci_barcode_barcode',
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('arguments', array(
	'nid' => array(
	  'default_action' => 'ignore',
	  'style_plugin' => 'default_summary',
	  'style_options' => array(),
	  'wildcard' => 'all',
	  'wildcard_substitution' => 'All',
	  'title' => '',
	  'breadcrumb' => '',
	  'default_argument_type' => 'fixed',
	  'default_argument' => '',
	  'validate_type' => 'none',
	  'validate_fail' => 'not found',
	  'break_phrase' => 1,
	  'not' => 0,
	  'id' => 'nid',
	  'table' => 'node',
	  'field' => 'nid',
	  'validate_user_argument_type' => 'uid',
	  'validate_user_roles' => array(
		'2' => 0,
	  ),
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	  'default_options_div_prefix' => '',
	  'default_argument_fixed' => '',
	  'default_argument_user' => 0,
	  'default_argument_php' => '',
	  'validate_argument_signup_status' => 'any',
	  'validate_argument_signup_node_access' => 0,
	  'validate_argument_node_type' => array(
		'merci_reservation' => 0,
		'dv_camera' => 0,
		'page' => 0,
		'story' => 0,
	  ),
	  'validate_argument_node_access' => 0,
	  'validate_argument_nid_type' => 'nid',
	  'validate_argument_vocabulary' => array(
		'2' => 0,
	  ),
	  'validate_argument_type' => 'tid',
	  'validate_argument_transform' => 0,
	  'validate_user_restrict_roles' => 0,
	  'validate_argument_php' => '',
	),
  ));
  $handler->override_option('filters', array(
	'sub_type' => array(
	  'operator' => 'in',
	  'value' => array(
		'1' => '1',
	  ),
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'sub_type',
	  'table' => 'merci_reservation_item_node',
	  'field' => 'sub_type',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('access', array(
	'type' => 'none',
  ));
  $handler->override_option('cache', array(
	'type' => 'none',
  ));
  $handler->override_option('title', 'Select Layout');
  $handler->override_option('items_per_page', 0);
  $handler = $view->new_display('mailing', 'Barcode Labels', 'mailing_1');
  $handler->override_option('attachment_position', 'before');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', FALSE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'page_1' => 'page_1',
	'default' => 0,
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/merci/manage/labels/print');
  $handler->override_option('menu', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => '0',
	'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));

 
  $views[$view->name] = $view;
  
  $view = new view;
  $view->name = 'merci_barcode_labels_search';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
	'title' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'exclude' => 0,
	  'id' => 'title',
	  'table' => 'node',
	  'field' => 'title',
	  'relationship' => 'none',
	),
	'field_merci_barcode_barcode' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'label_type' => 'none',
	  'format' => 'default',
	  'multiple' => array(
		'group' => TRUE,
		'multiple_number' => '',
		'multiple_from' => '',
		'multiple_reversed' => FALSE,
	  ),
	  'exclude' => 0,
	  'id' => 'field_merci_barcode_barcode',
	  'table' => 'node_data_field_merci_barcode',
	  'field' => 'field_merci_barcode_barcode',
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('filters', array(
	'sub_type' => array(
	  'operator' => 'in',
	  'value' => array(
		'1' => '1',
	  ),
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'sub_type',
	  'table' => 'merci_reservation_item_node',
	  'field' => 'sub_type',
	  'relationship' => 'none',
	),
	'keys' => array(
	  'operator' => 'optional',
	  'value' => '',
	  'group' => '0',
	  'exposed' => TRUE,
	  'expose' => array(
		'use_operator' => 0,
		'operator' => 'keys_op',
		'identifier' => 'keys',
		'label' => 'Search',
		'optional' => 1,
		'remember' => 0,
	  ),
	  'id' => 'keys',
	  'table' => 'search_index',
	  'field' => 'keys',
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('access', array(
	'type' => 'none',
  ));
  $handler->override_option('cache', array(
	'type' => 'none',
  ));
  $handler->override_option('title', 'Select Barcodes to Print');
  $handler->override_option('items_per_page', 25);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk');
  $handler->override_option('style_options', array(
	'grouping' => '',
	'override' => 1,
	'sticky' => 0,
	'order' => 'asc',
	'columns' => array(
	  'title' => 'title',
	  'field_merci_barcode_barcode' => 'field_merci_barcode_barcode',
	),
	'info' => array(
	  'title' => array(
		'sortable' => 0,
		'separator' => '',
	  ),
	  'field_merci_barcode_barcode' => array(
		'sortable' => 0,
		'separator' => '',
	  ),
	),
	'default' => '-1',
	'execution_type' => '1',
	'display_type' => '0',
	'hide_select_all' => 0,
	'skip_confirmation' => 0,
	'display_result' => 1,
	'merge_single_action' => 1,
	'selected_operations' => array(
	  'merci_barcode_labels_operations_update' => 'merci_barcode_labels_operations_update',
	  'node_assign_owner_action' => 0,
	  'merci_operations_update' => 0,
	  'views_bulk_operations_delete_node_action' => 0,
	  'node_mass_update:a27b9efabcd054685a549378b174ad11' => 0,
	  'system_message_action' => 0,
	  'views_bulk_operations_action' => 0,
	  'views_bulk_operations_script_action' => 0,
	  'node_make_sticky_action' => 0,
	  'node_make_unsticky_action' => 0,
	  'node_mass_update:c4d3b28efb86fd703619a50b74d43794' => 0,
	  'views_bulk_operations_fields_action' => 0,
	  'views_bulk_operations_taxonomy_action' => 0,
	  'views_bulk_operations_argument_selector_action' => 0,
	  'node_promote_action' => 0,
	  'node_mass_update:14de7d028b4bffdf2b4a266562ca18ac' => 0,
	  'node_mass_update:9c585624b9b3af0b4687d5f97f35e047' => 0,
	  'node_publish_action' => 0,
	  'system_goto_action' => 0,
	  'node_unpromote_action' => 0,
	  'node_mass_update:8ce21b08bb8e773d10018b484fe4815e' => 0,
	  'node_save_action' => 0,
	  'system_send_email_action' => 0,
	  'node_mass_update:0ccad85c1ebe4c9ceada1aa64293b080' => 0,
	  'node_unpublish_action' => 0,
	  'node_unpublish_by_keyword_action' => 0,
	),
	'views_bulk_operations_fields_action' => array(
	  'php_code' => 0,
	  'display_fields' => array(
		'_all_' => '_all_',
	  ),
	),
	'vbo_barcode_select' => '-',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/merci/manage/labels/search');
  $handler->override_option('menu', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  
  $views[$view->name] = $view;
  return $views;

}
